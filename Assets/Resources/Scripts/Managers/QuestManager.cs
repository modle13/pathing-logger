using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuestManager : MonoBehaviour {

    public static QuestManager manager;

    private List<Quest> quests = new List<Quest>();
    private int currentQuestIndex = 0;
    private Quest currentQuest;
    private GameObject questContainer;

    void Awake() {
        // singleton pattern
        if (manager == null) {
            manager = this;
        } else if (manager != this) {
            Destroy(gameObject);
        }
        questContainer = GameObject.Find("QuestContainer");
    }

    void Start() {
        DefineQuests();
        SetQuest();
    }

    void Update() {
        SetText();
        UpdateQuest();
    }

    void DefineQuests() {
        Dictionary<string, int> resources = ResourceCounter.counter.resourceGains;
        quests.Add(new CountingQuest("harvest trees by selecting the tree in the bottom-left target selector panel, then dragging to select target trees", resources, "wood", 1));
        quests.Add(new CountingQuest("harvest rocks with the rock target selector", resources, "rock", 2));
        quests.Add(new BuildingQuest("construct an additional storage box by selecting the storage box option in the bottom-right contruction selector panel", "storage", BuildingManager.manager.gameObject, 2));
        quests.Add(new UnlockingQuest("unlock the Sawyer; hit 'T' to open the technology tree to unlock", "sawyer", BuildingManager.manager.buildingSelectors));
        quests.Add(new BuildingQuest("construct the Sawyer by selecting the new sawyer option in the bottom-right contruction selector panel", "sawyer", BuildingManager.manager.gameObject, 1));
        quests.Add(new CountingQuest("produce planks with the Sawyer building", resources, "plank", 2));
        quests.Add(new DestructionQuest("destroy remaining trees", "tree"));
        quests.Add(new DestructionQuest("destroy remaining rocks", "rock"));
    }

    void SetQuest() {
        if (quests.Count - 1 >= currentQuestIndex) {
            currentQuest = quests[currentQuestIndex];
            currentQuest.Init();
        }
    }

    void SetText() {
        questContainer.GetComponent<TextMeshProUGUI>().text = "Quest: " +
            (currentQuest == null ? "" : currentQuest.GetRepr());
    }

    void UpdateQuest() {
        if (currentQuest == null) {
            return;
        }
        if (currentQuest.IsComplete()) {
            currentQuest = null;
            currentQuestIndex += 1;
            SetQuest();
        }
    }
}
