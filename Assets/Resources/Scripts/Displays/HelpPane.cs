using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using TMPro;

public class HelpPane : MonoBehaviour {

    private GameObject helpContainer;
    private string help = "\nhotkeys:\n" +
        "    H : help\n" +
        "    T : toggle tech tree\n" +
        "    L : toggle message log\n" +
        "    Q : toggle quest log\n" +
        "    1 : select tree harvester\n" +
        "    2 : select rock harvester\n" +
        "    0 : select storage builder";

    void Start() {
        transform.Find("HelpContainer").gameObject.GetComponent<TextMeshProUGUI>().text = help;
    }
}
